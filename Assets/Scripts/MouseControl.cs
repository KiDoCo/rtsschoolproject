﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    public static MouseControl Controller;

    [SerializeField, Range(0, 50)]
    private float scrollSpeed;

    private void Awake()
    {
        Controller = this;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        GroundHitCheck();
        CameraMovement();

        if (Input.GetKeyDown(KeyCode.Space))
            Camera.main.transform.position = Vector3.zero + Vector3.up * 30;
    }

    private void GroundHitCheck()
    {
        if (Input.GetMouseButtonDown(0))
        {

            Ray testray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            int layermask = LayerMask.GetMask("Ground");

            if (Physics.Raycast(testray, out hit, Mathf.Infinity, layermask, QueryTriggerInteraction.Collide))
                if (hit.transform != null)
                {
                    Material tempMat = new Material(hit.transform.GetComponent<Renderer>().sharedMaterial);
                    Debug.DrawRay(hit.point, Vector3.up, Color.blue, 5.0f);
                    tempMat.color = Color.red;
                    hit.transform.GetComponent<Renderer>().sharedMaterial = tempMat;
                }

        }

    }

    private void CameraMovement()
    {
        float speedIncrement = 0;

        if (Input.mousePosition.y >= Screen.height * 0.95f)
        {
            speedIncrement = (Input.mousePosition.y - Screen.height * 0.95f) / 15;
            Camera.main.transform.Translate(Vector3.forward * Time.deltaTime * scrollSpeed * speedIncrement, Space.World);
        }
        else if (Input.mousePosition.y <= Screen.height * 0.05f)
        {
            speedIncrement = (Screen.height * 0.05f - Input.mousePosition.y) / 15;
            Camera.main.transform.Translate(-Vector3.forward * Time.deltaTime * scrollSpeed * speedIncrement, Space.World);
        }

        if (Input.mousePosition.x >= Screen.width * 0.95f)
        {
            speedIncrement = (Input.mousePosition.x - Screen.width * 0.95f) / 15;
            Camera.main.transform.Translate(Vector3.right * Time.deltaTime * scrollSpeed * speedIncrement, Space.World);
        }
        else if (Input.mousePosition.x <= Screen.width * 0.05f)
        {
            speedIncrement = (Screen.width * 0.05f - Input.mousePosition.x) / 15;
            Camera.main.transform.Translate(-Vector3.right * Time.deltaTime * scrollSpeed * speedIncrement, Space.World);
        }
    }

}
